import setuptools

Path(__file__).parent.parent / 'app/predict/model'



setuptools.setup(
    name="models",
    version="0.0.1",
    packages=setuptools.find_packages(),
    install_requires=[
        'rules_spacy_model==1.9.18',
        'fastcore==1.2.5',
        'pydantic~=1.7'
    ],
    tests_require=["nose"],
    test_suite='nose.collector',
    include_package_data=True
)
