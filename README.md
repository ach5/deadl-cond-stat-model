![File structure](delete_me.png "Logo Title Text 1")

pip install -r requirements.txt --extra-index-url='https://admin:PPg8)JQjhJnd7VzRChcLVVUV@pypi.cteleport.com'

# Sample train code (fake code)
```python
from models import Myarch
from models import metric1
from models import Params
from models import dataloader 

params = Params()

list_of_callbacks = []

learner = Myarch(
    dataloader,
    **params.model.__dict__,
    metrics=[metric1],
    callbacks=list_of_callbacks
)

```

# Sample inference code - low level (fake code)
```python
import torch
from models import Myarch
from models import pipelineX
model = Myarch()
model.load_state_dict(torch.load("artefacts/weights.pickle"))
prepared_x = pipelineX(input_from_api)
predictions = Myarch(prepared_x)
```

# Sample inference with function - for use in API (fake code)
```python
from models import predict
from models import pipelineX
prepared_x = pipelineX(input_from_api)
predictions = predict(prepared_x)

```

# Upload to Pypi

Change version number
```bash
nosetests && bumpversion --message="Version message goes here" patch && git push
```

Upload to PyPi
```bash
nosetests && python setup.py sdist upload -r cteleport
```