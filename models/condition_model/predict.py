from pathlib import Path
from models.condition_model.architecture.pipeline import pipelineX
from models._parent_model.mock import req
from models._parent_model import load_model

path = Path(__file__).parent.parent.parent.parent / "artefacts/data"
model = load_model(path / 'ConditionModel.pickle')
sent, category = pipelineX(req)
pred = model.predict(sent, category)
print(pred)


def get_predict():
    pass
