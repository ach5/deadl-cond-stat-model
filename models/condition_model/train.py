# TODO: подумать как сюда можно прикрутить метрики
# from models.condition_model.architecture import metric1
from models.condition_model.architecture import ConditionModel
from models.condition_model.train_data.prepare import df
from models._parent_model import load_model
from pathlib import Path

path_ = Path(__file__).parent.parent.parent.parent / "artefacts/data"
model = ConditionModel(df)
model.save(path_ / 'ConditionModel.pickle')

model_loaded = load_model(path_ / 'ConditionModel.pickle')

print(model_loaded.df == model.df)
