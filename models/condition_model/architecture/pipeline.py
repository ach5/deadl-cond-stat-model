from fastcore.transform import Pipeline
from rules_data.transform import Spacify
from models._parent_model.rulez_data import Sentify
from models._parent_model.transform import ToDf, DeadlineRequestTransform
import sys

sys.path.append('../')


class CondtionDf(ToDf):
    model_type = 'condition'


pipeline_train = Pipeline([Spacify(), Sentify(), CondtionDf()])

pipelineX = Pipeline([DeadlineRequestTransform()])
