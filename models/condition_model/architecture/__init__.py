# That's where you store the model architecture. If there are multiple models, put them in separate folders.

from models._parent_model import AbstractModel
import pickle


class ConditionModel(AbstractModel):
    # TODO: нужно чтобы объект возвращался rules_data.models.deadlines_conditions.PredictedCondition
    # noinspection PyUnresolvedReferences
    """
            def predict
            Parameters
            ----------
            sent: str
            category: str

            Returns
            -------
            predict: dict
            _______
            >>> s_model = ConditionModel(df_cond)
            >>> s_model.predict('charge [[Money]] for cancel refund', '/subrules[0]/cancellation/fee/values')
            {'category': 'no_condition', 'weight': 0.94}
        """
    model_type = 'condition'


