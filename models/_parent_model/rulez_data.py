import spacy
from typing import List
import re
from fastcore.transform import Transform
from pydantic import BaseModel, BaseConfig
from rules_data import snake_to_camel
from enum import Enum
from spacy.tokens.doc import Doc


# TODO: перенести в репо
class Deadline(str, Enum):
    """
    Values appear as they are in the interpretation protocol, but converted to camelCase.
    `time_before_flight` does not exist in the interpretation protocol. Instead, we set it
    if in the interpretation protocol there is `value` and `units` key.
    """
    no_deadline = 'noDeadline'
    day_before_flight = 'dayBeforeFlight'
    day_of_flight = 'dayOfFlight'
    day_after_flight = 'dayAfterFlight'
    noshow = 'noShow'
    after_departure = 'afterDepartureTime'
    time_before_flight = 'timeBeforeFlight'

    # @root_validator(pre=True)
    # @validator('noshow')
    # def noshow_val(cls, v):
    #     if v == 'no_show':
    #         return 'noShow'
    @classmethod
    def from_str(cls, string):
        if string in ['value', 'units']:
            return cls('timeBeforeFlight')
        return cls(snake_to_camel(string))


class Condition(str, Enum):
    """
    All values are exactly as they appear in the interpretation protocol

    EXAMPLES:
    >>> Condition('noCondition').value
    'noCondition'

    >>> Condition.from_str('if_not_refundable').value
    'ifNotRefundable'
    """
    no_condition = "noCondition"
    marine_only = "marineOnly"
    fare_basis = "fareBasis"
    point_of_sales = "pointOfSales"
    domestic = "domestic"
    originating = "originating"
    originating_area = "originatingArea"
    destination = "destination"
    destination_area = "destinationArea"
    ticketing_in = "ticketingIn"
    ticketing_date = "ticketingDate"
    cancellation_date = "cancellationDate"
    carriers = "carriers"
    segment = "segment"
    if_not_refundable = "ifNotRefundable"
    gate_no_show = "gateNoShow"
    after_departure = "afterDeparture"
    between = "between"
    after_checkin = "afterCheckin"

    @classmethod
    def from_str(cls, string):
        return cls(snake_to_camel(string))


class PredictedCondition(BaseModel):
    prediction: Condition = Condition.no_condition
    weight: float = 1


class PredictedDeadline(BaseModel):
    prediction: Deadline = Deadline.no_deadline
    weight: float = 1

    # @validator('noshow')
    # def noshow_val(cls, v):
    #     if v == 'no_show':
    #         return 'noShow'


# TODO: Это надо перенести в репо rules_data
# from rules_data.models.datapoints import DataPoint


class DataPoint(BaseModel):
    rule_id: str
    sentence: spacy.tokens.span.Span
    sents_id: int
    category: str
    conditions: List[Condition]
    deadline: Deadline

    class Config(BaseConfig):
        arbitrary_types_allowed = True


# noinspection PyTypeChecker
def to_path(category):
    """
        Parameters
        ----------
        category: str

        Returns
        -------
        category_path: List[str]

        Examples
        --------
        >>> to_path('/subrules[0]/cancellation/fee/values')
        [0, 'cancellation', 'fee', 'values']
    """
    maper = {'fareCombination': 'fare_combination',
             'noShow': 'no_show',
             'feePer': 'fee_per',
             }

    if re.search(r"\[\d\]", category):
        sub_id = re.search(r"\[\d\]", category).group(0)
        sub_id = int(re.search(r"\d", sub_id).group(0))
        category = re.sub(r"\[\d\]", '', category)
        category_path = [sub_id] + [x for x in category.split('/') if len(x) > 0]
    else:
        category_path = [x for x in category.split('/') if len(x) > 0]
    category_path = [x for x in category_path if x != 'subrules']
    category_path = [maper.get(x, x) for x in category_path]
    return category_path


# noinspection PyTypeChecker
def get_entity(entity, category, indices, subrules, rule_id_):
    """
    Parameters
    ----------
    entity: str
    category: str
    indices: List[int]
    subrules: List[dict]
    rule_id_: str
    Returns
    -------
    :class: List

    Examples
    --------
    >>> get_entity(\
        entity='condition',\
        category='/subrules[0]/cancellation/fee/values', \
        indices=[1], \
        subrules=[{'cancellation': {'fare_combination': 'strictest', 'fee': {'values': [{'value': 300.0, 'ccy': 'USD'},\
            {'value': 300.0, 'ccy': 'USD', 'condition': {'after_departure': True}}]}},\
            'allow_auto_refund': True}],\
        rule_id_='_'
    )
    ['after_departure']

    >>> get_entity(\
        entity='condition',\
        category='/markup/fee/values', \
        indices=[1], \
        subrules=[{'cancellation': {'fare_combination': 'strictest', 'fee': {'values': [{'value': 300.0, 'ccy': 'USD'},\
            {'value': 300.0, 'ccy': 'USD', 'condition': {'after_departure': True}}]}},\
            'allow_auto_refund': True}],\
        rule_id_='_'
    )
    []

    >>> get_entity(
    ... entity='deadline',
    ... category='/subrules[0]/cancellation/fee/values',
    ... indices=[1],
    ... subrules=[{'no_show': {'definition': {'no_show': False, 'value': 2.0, 'units': 'hours', 'day_of_flight': False, 'day_before_flight': False, 'day_after_flight': False}}, 'cancellation': {'fare_combination': 'strictest', 'fee': {'values': [{'value': 150.0, 'ccy': 'USD'}, {'value': 150.0, 'ccy': 'USD', 'deadline': {'no_show': True, 'day_of_flight': False, 'day_before_flight': False, 'day_after_flight': False}}]}}, 'allow_auto_refund': True}],
    ... rule_id_='_'
    ... )
    ['day_after_flight', 'day_before_flight', 'day_of_flight', 'no_show']
    """
    if not category.startswith('/subrules'):
        return []

    subrules_ = subrules.copy()
    #     print(category)
    #     print(to_path(category))
    #     print(subrules)
    #     print('-'*33)
    for step in to_path(category):
        if type(step) == str:
            if step not in subrules_:
                print(f'{rule_id_}, get_conditions: "{category}" not in {subrules}')
                return []
        # noinspection PyTypeChecker
        subrules_ = subrules_[step]
    conditions = [subrules_[i].get(entity) for i in indices]
    conditions = [list(x.keys()) for x in conditions if x is not None]
    conditions = [item for sublist in conditions for item in sublist]
    return sorted(list(set(conditions)))


class Sentify(Transform):

    def encodes(self, doc: Doc) -> List[DataPoint]:
        """Converts list[Doc] into a List[List[DataPoint]]
        """

        if not doc._.interpretation.refund_in_prod:
            print(f'{doc._.rule_id} parse_doc: not refund_in_prod ')
            return []

        # assert len(list(doc.sents)) == len(doc._.annotation.sentences), \
        #     f'parse_doc: Кол-во предложений не совпадает {doc._.rule_id}'
        if not len(list(doc.sents)) == len(doc._.annotation.sentences):
            print(f'{doc._.rule_id} parse_doc: Кол-во предложений не совпадает')
            return []

        subrules = doc._.interpretation.subrules

        data_points = []

        for s_i, sent in enumerate(doc._.annotation.sentences):
            sent_categories = sent.category.user_entry
            for data in [[x.category_, x.indices, subrules] for x in sent_categories]:
                conditions = get_entity(*(['condition'] + data + [doc._.rule_id]))
                deadlines = get_entity(*(['deadline'] + data + [doc._.rule_id]))

                conditions = [Condition.from_str(x) for x in conditions]
                deadline = [Deadline.from_str(x) for x in deadlines]

                if len(deadline):
                    deadline = deadline[0]
                else:
                    deadline = Deadline('noDeadline')
                if not conditions:
                    conditions = [Condition('noCondition')]
                # TODO: проверить насколько точно работает проверка DataPoint в pydantic
                # подумать как это будет работать для модели категорий
                data_point = DataPoint(
                    rule_id=doc._.rule_id,
                    sentence=list(doc.sents)[s_i],
                    sents_id=s_i,
                    category=data[0],
                    conditions=conditions,
                    deadline=deadline
                )

                data_points.append(data_point)
        return data_points
