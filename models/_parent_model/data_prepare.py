from rules_data.models import FareRuleAnnotation_v2, FareRuleInterpretation_v1
import json


def get_data(annotation_path, interpretations_path):
    anns = json.load(open(annotation_path, 'r'))
    interps = json.load(open(interpretations_path, 'r'))
    common_ids = list(set([x['id'] for x in anns]) & set([interps[x]['id'] for x in interps]))
    interps = [interps[k] for k in common_ids]
    anns = sorted([x for x in anns if x['id'] in common_ids], key=lambda x: common_ids.index(x['id']))
    print(len(anns), len(interps))
    annotation = [FareRuleAnnotation_v2(**a) for a in anns]
    interpretation = [FareRuleInterpretation_v1(**i) for i in interps]
    data = tuple([list(x) for x in zip(annotation, interpretation)])
    return data
