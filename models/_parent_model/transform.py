from typing import Tuple
from fastcore.transform import ItemTransform
import pandas as pd
from rules_spacy_model import nlp


class ToDf(ItemTransform):
    model_type = '_'

    def is_category_ok(self, category):
        bad_words = ['markup', 'change']
        return all([b_word not in category for b_word in bad_words])

    def encodes(self, from_sentify) -> pd.DataFrame:
        from_sentify = [item for sublist in from_sentify for item in sublist]
        # len(from_sentify) == len of all DataPoints
        result = []
        for datapoint in from_sentify:
            if self.is_category_ok(datapoint.category):
                sent_cat = {'sent_cat': '_'.join([datapoint.sentence._.normalize(), datapoint.category])}

                if self.model_type == 'condition':
                    model_type = {'condition': ' '.join(x.name for x in datapoint.conditions)}
                    result.append({**sent_cat, **model_type})
                elif self.model_type == 'deadline':
                    model_type = {'deadline': datapoint.deadline.name}
                    result.append({**sent_cat, **model_type})

        # noinspection PyTypeChecker
        return pd.DataFrame.from_dict(result, orient='columns')


class DeadlineRequestTransform(ItemTransform):

    def encodes(self, req: dict) -> Tuple[str, str]:
        sent_ind = req['user_entry']['sent_index']
        letters_range = req['sentences'][sent_ind]

        sentence = req['raw_text'][slice(*letters_range)]

        return nlp(sentence)._.normalize(), req['user_entry']['category']
