import pickle


class AbstractModel:
    model_type = '_'

    def __init__(self, df):
        """

        Parameters
        ----------
        df: pd.DataFrame - training dataframe
            columns:
                sent_cat: str
                conditions [or] deadlines: str
        """
        self.df = df

    def predict(self, sent, category):
        request = '_'.join([sent, category])
        raw_values = self.df[self.df.sent_cat == request][self.model_type]
        predict = raw_values.value_counts() / len(raw_values)
        predict = predict[:1]
        predict = {self.model_type: predict.index[0], 'weight': predict.values[0]}
        return predict

    def save(self, path):
        with open(path, 'wb') as handle:
            pickle.dump(self, handle)

def load_model(path):
    with open(path, 'rb') as f:
        model_loaded = pickle.load(f)
    return model_loaded
