from pydantic import BaseSettings


class Params(BaseSettings):
    class pipelineX:
        param1 = None
        param2 = None

    class pipelineY:
        param = None

    class model:
        param4 = None
