# TODO: подумать как сюда можно прикрутить метрики
# from models.deadline_model.architecture import metric1
from models.deadline_model.architecture import DeadlineModel
from models.deadline_model.train_data.prepare import df
from models._parent_model import load_model
from pathlib import Path

path_ = Path(__file__).parent.parent.parent.parent / "artefacts/data"
model = DeadlineModel(df)
model.save(path_ / 'DeadlineModel.pickle')

model_loaded = load_model(path_ / 'DeadlineModel.pickle')

print(model_loaded.df == model.df)
