from pathlib import Path

from models.deadline_model.architecture.pipeline import pipeline_train
from models._parent_model.data_prepare import get_data

path_ = Path(__file__).parent.parent.parent.parent / "artefacts/data"

data_pipe = get_data(path_ / 'annotations_light.json', path_ / 'interpretations_light.json')
df = pipeline_train(data_pipe)
