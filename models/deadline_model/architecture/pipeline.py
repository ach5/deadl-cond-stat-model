from fastcore.transform import Pipeline
from rules_data.transform import Spacify
from models._parent_model.rulez_data import Sentify
from models._parent_model.transform import ToDf, DeadlineRequestTransform
import sys

sys.path.append('../')


class DeadlineDf(ToDf):
    model_type = 'deadline'


pipeline_train = Pipeline([Spacify(), Sentify(), DeadlineDf()])

pipelineX = Pipeline([DeadlineRequestTransform()])
