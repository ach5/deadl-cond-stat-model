# That's where you store the model architecture. If there are multiple models, put them in separate folders.

from models._parent_model import AbstractModel


class DeadlineModel(AbstractModel):
    # noinspection PyUnresolvedReferences
    """
            def predict
            Parameters
            ----------
            sent: str
            category: str

            Returns
            -------
            predict: dict
            _______
            >>> s_model = DeadlineModel(df_deadline)
            >>> s_model.predict('charge [[Money]] for cancel refund', '/subrules[0]/cancellation/fee/values')
            {'deadline': 'no_deadline', 'weight': 1.0}
        """
    model_type = 'deadline'
